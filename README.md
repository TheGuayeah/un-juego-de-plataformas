# Un juego de plataformas

Para éste proyecto he ido un poco escaso de tiempo pero finalmente he podido completarlo según las indicaciones del ejemplo con algunos pequeños cambios.
He añadido comentarios al código y un MainMenú desde el que se inicia el juego quitando la cuenta atrás del ejemplo porque me causaba algunos errores
y bajo mi punto de vista interrumpe la experiencia de juego. Por la misma razón, en lugar de reiniciar la escena con un botón he decidido que se reinicie
automáticamente al morir, como se muestra en el video.

También he cambiado los controles para que funcione al mismo tiempo con las Arrow Keys, WASD, y con los joysticks de cualquier mando de consola utilizando
los Input.GetAxis("Horizontal/Vertical").

Finalmente he aumentado la velocidad de movimiento y de rotación del jugador porque los valores que vienen en el ejemplo son demasiado bajos y el personaje
prácticamente no se podía mover, o por lo menos no se apreciaba el movimiento. Al alcanzar la gema el jugador vuelve al Main Menú desde el que puede volver a jugar.
