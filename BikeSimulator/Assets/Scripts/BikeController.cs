﻿using System;
using UnityEngine;

public class BikeController : MonoBehaviour
{
    public float LinearSpeed = 8;
    public float RotationSpeed = 50;

    public Transform BackWheel;
    public bool isGrounded;
    public Action OnKilled;
    public Action OnReachedEndOfLevel;

    private Rigidbody2D rb;
    private float wheelRadius;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        wheelRadius = GetComponent<CircleCollider2D>().radius;
    }

    private void Update()
    {
        isGrounded = Physics2D.OverlapCircleAll(BackWheel.position, wheelRadius * 1.1f).Length > 1;

        /*Usando los Axis Horizontal y Vertical conseguimos que el juego se más escalable, ya que
        también sirben teclado y mando en las demás plataformas.*/
        if (Input.GetAxis("Horizontal") < 0)
        {
            MoveBackward();
        }

        if (Input.GetAxis("Horizontal") > 0)
        {
            MoveForward();
        }

        if (Input.GetAxis("Vertical") > 0)
        {
            DoStoppie();
        }

        if (Input.GetAxis("Vertical") < 0)
        {
            DoWheelie();
        }
    }

    /// <summary>
    /// Mover el personaje hacia el frente.
    /// </summary>
    private void MoveForward()
    {
        if (isGrounded)
        {
            Vector3 right = transform.right;
            rb.velocity += new Vector2(right.x * LinearSpeed, right.y * LinearSpeed) * Time.deltaTime;
        }
    }

    /// <summary>
    /// Mover el personaje hacia atrás.
    /// </summary>
    private void MoveBackward()
    {
        if (isGrounded)
        {
            Vector3 right = transform.right;
            rb.velocity -= new Vector2(right.x * LinearSpeed, right.y * LinearSpeed) * Time.deltaTime;
        }
    }

    /// <summary>
    /// Hacer un caballito con la moto.
    /// </summary>
    private void DoWheelie()
    {
        rb.MoveRotation(rb.rotation + RotationSpeed * Time.deltaTime);
    }

    /// <summary>
    /// Frenar en seco levantando la rueda de atrás. De esta manera se considera que el jugador ya no está tocando el suelo.
    /// </summary>
    private void DoStoppie()
    {
        rb.MoveRotation(rb.rotation - RotationSpeed * Time.deltaTime);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("EndOfLevel"))
        {
            //Si el personaje colisiona con la gema ("EndOfLevel") gana la partida.
            OnReachedEndOfLevel?.Invoke();
        }
        else
        {
            //Si el personaje colisiona con cualquier cosa sin tag se considera muerto.
            OnKilled?.Invoke();
        }
    }
}
