﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    public BikeController BikeController;
    public Text TimeText;
    public Text RecordText;

    private float initialTime;
    private float recordTime;
    private int secondsToStart = 3;

    private void Start()
    {
        TimeText.text = RecordText.text = string.Empty;

        BikeController.OnKilled += RestartLevel;
        BikeController.OnReachedEndOfLevel += EndGame;

        if(PlayerPrefs.HasKey("recordLevel" + SceneManager.GetActiveScene().buildIndex))
            recordTime = PlayerPrefs.GetFloat("recordLevel" + SceneManager.GetActiveScene().buildIndex, 0);

        if (recordTime > 0)
            RecordText.text = "Record: " + recordTime.ToString("00.00");
    }

    private void Update()
    {
        if (BikeController.enabled)
        {
            TimeText.text = "Time: " + (Time.time - initialTime).ToString("00.00");
        }
    }

    public void StartGame()
    {
        TimeText.text = secondsToStart.ToString();
        InvokeRepeating(nameof(Countdown), 1, 1);
    }

    private void Countdown()
    {
        secondsToStart--;
        if (secondsToStart <= 0)
        {
            CancelInvoke();
            OnGameStarted();
        }
        else
            TimeText.text = secondsToStart.ToString();
    }

    /// <summary>
    /// Reiniciar el nivel, cuando el jugador muere.
    /// </summary>
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnGameStarted()
    {
        initialTime = Time.time;
        TimeText.text = string.Empty;
    }

    /// <summary>
    /// Terminar la partida cuando el jugador gana.
    /// </summary>
    private void EndGame()
    {
        TimeText.text = (Time.time - initialTime).ToString("00.00");

        if ((Time.time - initialTime) < recordTime)
        {
            TimeText.text = "NEW RECORD! " + (Time.time - initialTime).ToString("00.00");
            PlayerPrefs.SetFloat(key: "recordLevel" + SceneManager.GetActiveScene().buildIndex, value: Time.time - initialTime);
        }
        else
        {
            TimeText.text = "FINAL! " + (Time.time - initialTime).ToString("00.00");
        }
        SceneManager.LoadScene("Menu");
    }
}
